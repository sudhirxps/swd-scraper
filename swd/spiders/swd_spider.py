from bs4 import BeautifulSoup

from scrapy.http import FormRequest
from scrapy.spider import BaseSpider
from swd.items import SwdItem
from scrapy.shell import inspect_response

HEADERS = {
    # 'X-MicrosoftAjax': 'Delta=true',
    # 'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.76 Safari/537.36'
}
URL = 'http://www.bits-pilani.ac.in:12349/StudentSearch.aspx'


class ExitRealtySpider(BaseSpider):
    name = "swd"

    allowed_domains = ["www.bits-pilani.ac.in"]
    start_urls = [URL]

    def parse(self, response):
        # submit a form (first page)
        self.data = {}
        for form_input in response.css('form#form1 input'):
            name = form_input.xpath('@name').extract()[0]
            try:
                value = form_input.xpath('@value').extract()[0]
            except IndexError:
                value = ""
            self.data[name] = value

        
        self.data['__EVENTTARGET'] = 'searchResultGridView'
        self.data['__EVENTARGUMENT'] = 'Page$1'

        return FormRequest(url=URL,
                           method='POST',
                           callback=self.parse_page,
                           formdata=self.data,
                           meta={'page': 1},
                           dont_filter=True,
                           headers=HEADERS)

    def parse_page(self, response):
        current_page = response.meta['page'] + 1

        soup = BeautifulSoup(response.body)

        # inspect_response(response,self)

        # parse agents (TODO: yield items instead of printing)
        for agent in response.xpath('//table[@id="searchResultGridView"]/tr')[1:]:
            try:
              item = SwdItem()
          
              tds = agent.xpath('td')
              # inspect_response(response,self)
              item['id_no'] =  tds[0].xpath('.//text()').extract()[0]
              item['name'] = tds[1].xpath('.//text()').extract()[0]
              item['hostel'] = tds[2].xpath('.//text()').extract()[0]
              item['room'] = tds[3].xpath('.//text()').extract()[0]
              item['sex'] = tds[4].xpath('.//text()').extract()[0]

              yield item

            except IndexError:
              # Ignored temporarily
              pass        

        # request the next page
        data = {
            '__EVENTARGUMENT': 'Page$%d' % current_page,
            '__EVENTVALIDATION': soup.find('input',{'id':'__EVENTVALIDATION'})['value'],
            '__VIEWSTATE': soup.find('input',{'id':'__VIEWSTATE'})['value'],
            '__EVENTTARGET': 'searchResultGridView',            
        }

        yield FormRequest(url=URL,
                           method='POST',
                           formdata=data,
                           callback=self.parse_page,
                           meta={'page': current_page},
                           dont_filter=True,
                           headers=HEADERS)