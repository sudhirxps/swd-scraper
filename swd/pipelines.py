# -*- coding: utf-8 -*-
from scrapy.exceptions import DropItem
import MySQLdb
# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html


class MySQLSwdPipeline(object):

	# Move to settings.py file
	db = "swd"
	user = "root"
	passwd = "root"
	host = "localhost"

	def __init__(self):
		# Move to spider open function
		self.db = MySQLdb.connect(host=self.host,user=self.user,passwd=self.passwd,db=self.db)


	def process_item(self, item, spider):

		cur = self.db.cursor()

		query = """INSERT INTO students_no_key VALUES ('{0}','{1}','{2}','{3}','{4}')"""

		cur.execute(query.format(item['id_no'],item['name'],item['hostel'],item['room'],item['sex']))

		# Move to spider close function 
		self.db.commit()

		return item
