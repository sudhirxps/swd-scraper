# -*- coding: utf-8 -*-

# Scrapy settings for swd project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'swd'

SPIDER_MODULES = ['swd.spiders']
NEWSPIDER_MODULE = 'swd.spiders'
ITEM_PIPELINES = {
	'swd.pipelines.MySQLSwdPipeline':400,
}

# Crawl responsibly by identifying yourself (and your website) on the user-agent
USER_AGENT = 'swd (+http://sudhirxps.co.vu)'
