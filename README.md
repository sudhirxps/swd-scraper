# SWD Crawler #

Crawls the intranet site of BITS Pilani for gathering student information from the search feature on the site.

### Requires ###

* Scrapy 0.24.5
* beautifulsoup4 4.4.1
* requests 2.9.1
* mysqldb

### How to set up and use it? ###

* Clone the repository
```
$ git clone https://bitbucket.org/sudhirxps/swd-scraper.git

```
* Configure the database settings in the pipelines.py
```
    db = "<database-name>"
    user = "<database-username>"
    passwd = "<database-password>"
    host = "localhost"
```
* Set up the database and tables

```
 mysql> desc students;

+--------+--------------+------+-----+---------+-------+
| Field  | Type         | Null | Key | Default | Extra |
+--------+--------------+------+-----+---------+-------+
| id     | varchar(100) | YES  |     | NULL    |       |
| name   | varchar(100) | YES  |     | NULL    |       |
| hostel | varchar(10)  | YES  |     | NULL    |       |
| room   | varchar(10)  | YES  |     | NULL    |       |
| sex    | varchar(2)   | YES  |     | NULL    |       |
+--------+--------------+------+-----+---------+-------+

```
DDL for the database
```
CREATE TABLE `students` (
  `id` varchar(100) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `hostel` varchar(10) DEFAULT NULL,
  `room` varchar(10) DEFAULT NULL,
  `sex` varchar(2) DEFAULT NULL
)
```

Start crawling using the swd spider
```
scrapy crawl swd
```

### Known Issues ###

* Fails after scraping 10,000 records. The intranet site with no filters generates 500 pages with 20 records on each page. No further pagination links are available due to which the spider fails.